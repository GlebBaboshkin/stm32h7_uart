#ifndef __UART_STM_LIB_H

#define __UART_STM_LIB_H


#include "stm32h743xx.h"

/*
		������������ ���� ��� ��������� �������� USART_CR1
*/

#define    UART_EN    							 (0b1<<0)
#define 	 LowPower		 							 (0b1<<1)
#define    RecieverEn	 							 (0b1<<2)
#define    TransmitEn	 							 (0b1<<3)
#define  	 IDLE_IE		 							 (0b1<<4)
#define    RXFIFONotEmpty_IE				 (0b1<<5)
#define    TC_IE			 							 (0b1<<6)
#define    TXFIFONotFull_IE					 (0b1<<7)
#define    ParityError_IE						 (0b1<<8)
#define    OddParity								 (0b1<<9)
#define    ParityEnable							 (0b1<<10)	
#define    WakeByAddress						 (0b1<<11)
#define    M0												 (0b1<<12)
#define    MuteModeEn								 (0b1<<13)
#define    CharacterMatch_IE				 (0b1<<14)
#define    OverSample8bit						 (0b1<<15)

#define    DEDeasTime									16
#define    DEAssTime    							21

#define    RecTimeout_IE						 (0b1<<26)
#define    EndOfBlock_IE						 (0b1<<27)
#define 	 M1												 (0b1<<28)
#define    FIFOEn										 (0b1<<29)
#define    TXFIFOEn									 (0b1<<30)
#define    RXFIFOEn									 (0b1<<31)

/*
		������������ ���� ��� ��������� �������� USART_CR2
*/

#define   SlaveMod									(0b1<<0)
#define   NssInput									(0b1<<3)
#define		Addres_7bit								(0b1<<4)
#define   LIN_Break									(0b1<<5)
#define   LIN_BreakIE_En						(0b1<<6)
#define   LastBitOutput							(0b1<<8)
#define   CPHA											(0b1<<9)
#define   CPOL											(0b1<<10)
#define   SclkPinEn									(0b1<<11)

#define   StopBit										12

#define   Is_1_StopBit							0b00
#define   Is_0p5_StopBit						0b01
#define   Is_2_StopBit							0b10
#define   Is_1p5_StopBit						0b11

#define   LIN_En										(0b1<<14)
#define   Swap_RX_TX								(0b1<<15)
#define   RXInv											(0b1<<16)
#define   TXInv											(0b1<<17)
#define   DATAInv										(0b1<<18)
#define   MSBBit										(0b1<<19)
#define   AutoBode									(0b1<<20)

#define   AutoBodeMeasure						21

#define   ByStartBit								0b00
#define  	EdgeToEdge								0b01
#define   Frame0h7F									0b10
#define   Frame0h55									0b11

#define   ReciveTimeOutEn						(0b1<<23)

#define   Address										24

/*
		������������ ���� ��� ��������� �������� USART_CR3
*/

#define   ErrIE											(0b1<<0)
#define   IrDAEn										(0b1<<1)
#define   IrDALowPower							(0b1<<2)
#define   HalwDuplex								(0b1<<3)
#define   SmartNACKEn								(0b1<<4)
#define   SmartcardEn								(0b1<<5)
#define   DMARXEn										(0b1<<6)
#define   DMATXEn										(0b1<<7)
#define   RTSEn											(0b1<<8)
#define   CTSEn											(0b1<<9)
#define   CTSIE											(0b1<<10)
#define   OneSampleMode							(0b1<<11)
#define   OverRunDisable						(0b1<<12)
#define   DMA_ReceptionErrLock			(0b1<<13)
#define   DEMode										(0b1<<14)
#define   DEPolarity								(0b1<<15)

#define   SmartCardCounter           17

#define   WakeUpFromLP							 20

#define   WakeUpIE									  (1<<22)
#define   TXFifoIsFull_IE						(1<<23)
#define   TCBGTIE											(1<<24)

#define   RXFifoThreshLevel						25

#define   RXFifoIsFull_IE							(1<<28)

#define   TXFifoThreshLevel					29


/*
		USART_ISR
*/

#define  ParityErr		  	0
#define  FrameErr			   	1
#define  Noise				  	2
#define  Overrun			  	3
#define  IDLE						  4
#define  RXFifoNotEmpty		5
#define  TransComplete		6
#define  TXFifoNotFull		7
#define  LINBreak					8
#define  CTSInt						9
#define  CTS							10
#define  RXTimeout				11
#define  EndBlock					12
#define  SlaveUnderrun		13
#define  ABRateErr				14
#define  ABRateFlag				15
#define  Busy							16
#define  CharacterMatch		17
#define  SendBreak				18
#define  RXWUFromMute			19
#define  WUF							20
#define  TEACK						21
#define  REACK						22
#define  TXFifoEmp				23
#define  RXFifoFull				24
#define  TCGBT						25
#define  RXFifoThresh			26
#define  TXFifoThresh			27


void UART_Config(USART_TypeDef* UART,int Divider, int NumStopBits, int ParityEn, int TypeParity, int NumBits);

void UART_ClkSelect(USART_TypeDef* UART, int ClkSource);

int UART_FIFOReader(int start, int* Mass, int sizeMass, USART_TypeDef* UART);

void UART_GetFifoElements(int fifoFull);

void UART_SendBuffer(int* Buff, int BufLength, USART_TypeDef* UART);

#endif
