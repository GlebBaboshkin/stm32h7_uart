#include "stm32h7xx.h"                  // Device header

#include "uart_stm_lib.h"

//#define KerCLK			120000000
//#define BaudRate4		  2000000

//#define USARTDIV4		KerCLK/BaudRate4

int fifoElem;


void UART_GetFifoElements(int fifoFull)
{
		switch (fifoFull){
		case 0: fifoElem = 2; break;
		case 1: fifoElem = 4; break;
		case 2: fifoElem = 8; break;
		case 3: fifoElem = 12; break;
		case 4: fifoElem = 14; break;
		default: break;
	}
}

void UART_Config(USART_TypeDef* UART,int Divider, int NumStopBits, int ParityEn, int TypeParity, int NumBits)
{
	if (ParityEn == ParityEnable)
	{
			UART ->CR1 |=  ParityEn;
			
			if (TypeParity == OddParity) 
			{	
				UART ->CR1 |=  OddParity;
			}
			else {UART ->CR1 &=  ~OddParity;}
			
	} else {UART ->CR1 &=  ~ParityEn;}
	
	if (NumBits == 7)
	{
		UART ->CR1 |= M1;
	}
	else if (NumBits == 8)
	{
		UART ->CR1 &= ~(M1|M0);
	}
	else {UART ->CR1 |= M0;}
//	UART ->CR1 |=    FIFOEn| RecieverEn|
//											  ParityEnable|RecTimeout_IE|
//												OddParity|  M0| RXFIFONotEmpty_IE;
	
//		UART ->CR1 |=  RecieverEn|  FIFOEn| 
//											RXFIFOEn| TXFIFOEn| RecTimeout_IE;//|ParityEnable|
//											//	OddParity|  M0;
	
	
	UART->CR1 |= TransmitEn| FIFOEn| RecieverEn|RecTimeout_IE| RXFIFONotEmpty_IE;
	
	UART->RTOR |= 16;
	
	UART->CR2 |= ( NumStopBits << StopBit)|ReciveTimeOutEn;
	
	UART->CR3 &= ~(0b111<<RXFifoThreshLevel);
	
	UART->CR3 |= (0b100<<TXFifoThreshLevel)|(0b100<<RXFifoThreshLevel)|RXFifoIsFull_IE;
	
	UART->BRR |= Divider;  //������ ���� 62
	
	UART->CR1 |= UART_EN;
}

void UART_ClkSelect(USART_TypeDef* UART, int ClkSource)
{
	if (UART == USART1 || UART == USART6)
	{
		RCC->D2CCIP1R |= (ClkSource << 3);
	}
	else {RCC->D2CCIP1R |= (ClkSource);}
}


int UART_FIFOReader(int start, int* Mass, int sizeMass, USART_TypeDef* UART)
{
		int res;
	
		UART_GetFifoElements((UART->CR3&(0b111<<RXFifoThreshLevel))>>RXFifoThreshLevel);
	
		//UART->CR &= ~(1<<0);
	
		for (int i=start; i<(start+fifoElem); i++)
		{
			
			
			Mass[i]= UART->RDR&0xFF;
				
			res = i;
			
			if (res == sizeMass-1 | ((UART -> ISR&(1<<RXFifoNotEmpty)) != (1<<RXFifoNotEmpty))){
				break;
			}

		}
		
		//UART->CR |= (1<<0);
		
		return res+1;

}

void UART_SendBuffer(int* Buff, int BufLength, USART_TypeDef* UART)
{
	UART->CR1 |= TransmitEn;
	
	for (int i=0; i<BufLength; i++)
	{
		UART->TDR = Buff[i];
		
		while ((UART->ISR&(1<<27))!=(1<<27));
	}
	
			while ((UART->ISR&(1<<23))!=(1<<23));

			while ((UART->ISR&(1<<6))!=(1<<6));
	
	UART->CR1 &= ~TransmitEn;
}
