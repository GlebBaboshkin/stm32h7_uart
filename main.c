#include "stm32h7xx.h"                  // Device header
#include "stm32h743xx.h"
#include "clk_stm_lib.h"
#include "uart_stm_lib.h"
#include "port_stm_lib.h"
#include "isu_imit.h"
#include "regimes.h"

#define KerUartCLK				120000000
#define BaudRateU4		 		2000000
#define BaudRateU5				 115200 

#define USARTDIVU4				KerUartCLK/BaudRateU4
#define USARTDIVU5				KerUartCLK/BaudRateU5

Message RSS_Mes, NDC1_Mes, BSDP1_Mes, BSDP2_Mes, NDC2_Mes, BRUV_Mes; 

extern int TransmitCycle, Sended;
int last, last1;
int UartRxData[22], DataForCPU[22], DataFromCPU[20], RechangeData[16];
int T1=1, T2, T3;
int CRC1, CRC2;
extern int CopyInformation;
int CRC3;
int Regime, BREAK; 


void UART5_IRQHandler()
{
	if (((UART5->ISR&(1<<RXTimeout)) == (1<<RXTimeout)))		//���������� �� ��������� ��������� ���������� � ����
	{
			last1 = UART_FIFOReader(last1, DataFromCPU, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART5);
			GPIO_SetBit(Pin1, GPIOE); 
			if (DataFromCPU[0] == 21)
			{


				CRC3 = (0b01101001^(DataFromCPU[0] + (~DataFromCPU[1]<<4)))&0xFF;
				
				if (CRC3 == DataFromCPU[2])
				{
						for (int i = 3 ; i < last1-1;i++) 
						{ 
							RechangeData[i-3] = DataFromCPU[i];
						}					
					
					if (Regime != DataFromCPU[1])
					{
						
						Regime = DataFromCPU[1];
						//UART_SendBuffer(DataForCPU, sizeof(DataForCPU)/sizeof(DataForCPU[0]), UART5);	
					 	last1 = 0;
					}
					else {
								if (Regime != 0) BREAK = 1;
								//UART_SendBuffer(DataForCPU, sizeof(DataForCPU)/sizeof(DataForCPU[0]), UART5);	
								last1 = 0;
					}
					UART_SendBuffer(DataForCPU, sizeof(DataForCPU)/sizeof(DataForCPU[0]), UART5);	
				}
				else {last1 = 0; }
			}
			else {last1 = 0;}
			CRC3 = 0;
			GPIO_ResetBit(Pin1, GPIOE);
			UART5 -> ICR |= (1<<11);
	}
	
}



void UART4_IRQHandler()
{		


	if (((UART4->ISR&(1<<RXFifoNotEmpty)) == (1<<RXFifoNotEmpty)) && ((UART4->ISR&(1<<RXFifoThresh))!=(1<<RXFifoThresh)))		//���������� �� ��������� ��������� ���������� � ����
	{//GPIO_SetBit(Pin0, GPIOE);
		
			TIM5->CR1 &= ~(1<<0);
			TIM5->CNT = 0;
		//GPIO_ResetBit(Pin0, GPIOE);
		if (last == 0)																					//����� ��� �� �������, ��������� ������ ����� �� ����
		{
			//GPIO_SetBit(Pin0, GPIOE);
		
			Sended = 1;																						//������������� ��������� �������� ��������� �� ����
		
			//GPIO_ResetBit(Pin0, GPIOE);
			
			UART4->ICR |= (1<<RXTimeout);
			UART4->CR2 |= ReciveTimeOutEn;												//��������� ���������� �� ��������
		
			UART4 -> CR1 &= ~RXFIFONotEmpty_IE;										//��������� ���������� �� ������� �� ������� ����
			
			T1=1;																									//��������� � ��������� �������� ������� ������
			
			return;
		}
		else {					//������ ����� ���� ������ ������������, ����� � ���� ���� ����������, �� ��� �� ��������� �� ������. ������ ��������� �� ��������� ��������.


			last = UART_FIFOReader(last, UartRxData, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART4);

			if (T1 == 1 & T2 ==1 & T3 ==1)										//���� ������ ����� � ��1, ����� ������� ��� ���������� �������� � ����
			{

			
				for (int i=3; i<last-1; i++)
				{
					CRC2 +=  (CRC2<<2)+UartRxData[i];
				}
				CRC2 += (CRC2<<2);
				CRC2 ^= (CRC2>>8);
				CRC2 &= 0xFF;
			
				if (CRC2 == UartRxData[last-1])											//���� ��2 ���� ������� ������� � ��������� ������ � ���������
				{

					CRC1=0; CRC2 = 0;															//���������� ���
					last=0;																				//��������� ��������� ������
					CopyInformation = 1;													//��������� ��������� ���������� ����������
					//GPIO_SetBit(Pin0, GPIOE);											
					TIM5->CR1 |= (1<<0);														//��������� ������, ����� ���������� ����������
					UART4->CR2 |= ReciveTimeOutEn;												//��������� ���������� �� ��������
					UART4->ICR |= (1<<RXTimeout);
					//GPIO_ResetBit(Pin0, GPIOE);
					T2 = 0; T3 = 0;
					return;
				}
				else {
					CRC1=0; CRC2 = 0;															//���������� ���
					last=0;																				//��������� ��������� ������
					TIM5->CR1 |= (1<<0);														//��������� ������, ����� ���������� ����������
					UART4->CR2 |= ReciveTimeOutEn;												//��������� ���������� �� ��������
					UART4->ICR |= (1<<RXTimeout);			
					T2 = 0; T3 = 0;
					CopyInformation = 0;
				}
			}
			
			if (T1 == 1 & T2 == 0 & T3 == 0)									//�� ���� ������������ ������
			{

				last = 0;																				//��������� �� ����, � �� �� ��� �� ���������
				TIM5->CR1 |= (1<<0);														//�������� �������
				CopyInformation = 0;
				return;

			}
			
			if (T1 == 1 & T2 ==1 & T3 == 0)										//����� ������, �� ��1 �� �������
			{

				last = 0;																				//��������� �� ����, � �� �� ��� �� ���������
				TIM5->CR1 |= (1<<0);														//�������� �������
				CopyInformation = 0;
				T2=0;
				return;
			}
			
		}
		GPIO_ResetBit(Pin0, GPIOE);
	}
	
	if ((UART4->ISR&(1<<RXFifoThresh))==(1<<RXFifoThresh))
	{
		GPIO_SetBit(Pin1, GPIOE);
		last = UART_FIFOReader(last, UartRxData, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART4);
		GPIO_ResetBit(Pin1, GPIOE);
		if (T1 == 1  & T2 ==0 & T3 == 0)								//��������� ������ ����� � ������ �������
		{
		 if ((UartRxData[0]&0xFF) == ISU_addr)					//���� ������ ����� ��������� �� ���� ��������� � ������� ���� (��� ������ �� �����)
		  	{
				
		  		T2 = 1;																		//������ � ��������� ������ ���� ��������� ����, � �������� ��1
		
					if ((UART4->CR3 & (0b111<<RXFifoThreshLevel)) == (0b000<<RXFifoThreshLevel))
					{
						return;																	//�������������� �������� ����� ��� �� ����� � ����������� �� ������ ������. ���� ����� 
																										// ���� 12 � ������, ������� ���� ������ � ���� ���������� ����� ���������. ���� 2 �����, ������ �������.
					}
			  }
			  else {
					
			  	//last = 0; 																//� ��������� ������, ����� ����������� ����� ������ ���� �� ����
					GPIO_SetBit(Pin1, GPIOE);									//����� ���������� �� �������� ���������, � �� ��������� ���� ���������, ������� 
																										//������ ���� ����� ���������� ������ ��������� ����, ��� ���������� ������ ����
					UART4->CR2 |= ReciveTimeOutEn;												//��������� ���������� �� ��������
					UART4->ICR |= (1<<RXTimeout);

					GPIO_ResetBit(Pin1, GPIOE);
			  	return;																	//����� �� ����� ������ - ������ �����������. ��������� ����� �� ������ � ���� ��� ����� ������
																										// �������� � ���� �����. �� ��� ����� ������� ������ ��������.
			  }
		}
		
		if (T1 == 1 & T2 ==1 & T3 == 0)							//����� ������� ������, �� ��1 ��� �� ���������
		{

			CRC1 = UartRxData[0];
			CRC1 += (CRC1<<2)+(UartRxData[1]&0xFF);
			CRC1 += (CRC1<<2);
			CRC1 ^= (CRC1>>8);
			CRC1 &= 0xFF;

			
			if (CRC1 == (UartRxData[2]&0xFF))					//���� ����������� ����������� ����� ������� � ���, ��� ������ ������� ������ �� ����
			{

				T3=1;																		//������� � ��������� ��������������� ������ ����������, � ������� CRC2
				UART4->CR2 |= ReciveTimeOutEn;												//��������� ���������� �� ��������
			  UART4->ICR |= (1<<RXTimeout);

				return;																	//����� ���������� ����� ���������� �������� �����
				
			}
			else {
				
			//���� ��1 - �� �������
				
				CRC1 = 0;																//���������� ��1
				
				//last = 0;																//�������������� ����� ������ �� ������������� ������ ���� ����.
			  UART4->CR2 |= ReciveTimeOutEn;												//��������� ���������� �� ��������
				UART4->ICR |= (1<<RXTimeout);
					//������ ������ ������ ���������� ���������� �� �������� � ��������� ������ ���� ����. 

				return;																	//����� �� ���������� ����������
			}
			
		}


	}
	
	if ((UART4->ISR&(1<<RXTimeout))==(1<<RXTimeout))			//���������� ���������� �� ��������, ������ � ���� ��� ���� ����������, �� ����� �� ��������.
	{

//		last = UART_FIFOReader(last, UartRxData, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART4);
		GPIO_SetBit(Pin1, GPIOE);
		UART4 -> CR1 |= RXFIFONotEmpty_IE;									//��������� ���������� �� ������� �� ������� ����, ��� ��� ��� ����� ������� ����������
	
		UART4->ICR |= (1<<RXTimeout);
		if ((UART4->ISR&(1<<RXFifoNotEmpty)) == (1<<RXFifoNotEmpty))  UART4->RQR |= (1<<3);
		TIM5->CR1 |= (1<<0);
		GPIO_ResetBit(Pin1, GPIOE);
		return;
		
		
	}
 }


void TIM5_IRQHandler()
{
	if (TIM5->SR & (1<<0))
	{
		//GPIO_SetBit(Pin0, GPIOE);
		TIM5-> CR1 &= ~(1<<0);		//���������
		TIM5 -> CNT = 0;
		if (TransmitCycle == 16)
		{
				TransmitCycle = 0;
		}	
		
		TransmitCycle++;
		
		Sended = 0;
		

		
		TIM5->SR &= ~(1<<0);
		//GPIO_ResetBit(Pin0, GPIOE);
	}
}

void TIM_Init(TIM_TypeDef* Timer)
{
	
	Timer -> CR1 |= (0b00<<5);
	Timer -> CR2 &= 0x00000000;
	Timer -> ARR = (0x0FF0);//(0xC35);
	Timer -> DIER |= (1<<0);
	
}

void MessagesInit()
{
	SetMessage(RSS_addr, RSS_Length, &RSS_Mes);
	
	SetMessage(BSDP_addr, BSDP1_Length, &BSDP1_Mes);
	
	SetMessage(BSDP_addr, BSDP2_Length, &BSDP2_Mes);
	
	SetMessage(NDC_addr, NDC1_Length, &NDC1_Mes);
	
	SetMessage(NDC_addr, NDC2_Length, &NDC2_Mes);
	
	SetMessage(BRUV_addr, 12, &BRUV_Mes);
	
}

int main()
{
	
	CLK_Config();

	RCC->AHB4ENR = (1<<_PortA)| (1<<_PortB)|(1<<_PortE);
	
	RCC->APB1LENR |= (1<<_TIM5);
		
	UART_ClkSelect(UART4, USART_PCLK);
	
	UART_ClkSelect(UART5, USART_PCLK);
	
	RCC->APB1LENR |= (1<<_UART4) | (1<<_UART5);
	
	int PinsA[2] = {Pin0   						,   Pin1};
	int PinsAFunc[2] = {8   						,    8};
	int PinsASpeed[2] = {isFastFuriouse, isFastFuriouse};
	int PinsAMode[2]  = {isAlternate	, isAlternate};
	int PinsAPull[2] = {PullUp, PullOff};
	
	GPIO_PortConfig(PinsA, PinsAMode, PinsASpeed, PinsAFunc, PinsAPull,  GPIOA, (sizeof(PinsA)/sizeof(PinsA[0])));
	
	int PinsB[2] = {Pin5   						,   Pin6};
	int PinsBFunc[2] = {14   						,    14};
	int PinsBSpeed[2] = {isFastFuriouse, isFastFuriouse};
	int PinsBMode[2]  = {isAlternate	, isAlternate};
	int PinsBPull[2] = {PullOff, PullUp};
	
	GPIO_PortConfig(PinsB, PinsBMode, PinsBSpeed, PinsBFunc, PinsBPull, GPIOB, (sizeof(PinsB)/sizeof(PinsB[0])));	
	
	TIM_Init(TIM5);
	
	__NVIC_EnableIRQ(TIM5_IRQn);
	
	__NVIC_SetPriority(TIM5_IRQn, 2);
	
	GPIO_PinConfig(Pin1, isGenPort, isFastFuriouse, 0, PullOff,  GPIOE);
	GPIO_PinConfig(Pin0, isGenPort, isFastFuriouse, 0, PullOff,  GPIOE);
	
	UART_Config(UART4, USARTDIVU4, Is_1_StopBit, ParityEnable, OddParity, 9);
	
	UART_Config(UART5, USARTDIVU5, Is_1_StopBit, ~ParityEnable, ~OddParity, 8);
	
	__NVIC_EnableIRQ(UART4_IRQn);
	
	__NVIC_SetPriority(UART4_IRQn, 1);	
	
	__NVIC_EnableIRQ(UART5_IRQn);
	
	__NVIC_SetPriority(UART5_IRQn, 0);	
	
	MessagesInit();
	
	
	while (1)
	{
		
		switch (Regime){
			case 1:
			RemakeDataArray(&BRUV_Mes, RechangeData, 0, 1);
			MainCycleRun(); 	
			break;
			case 2:
			RemakeDataArray(&BRUV_Mes, RechangeData, 0, 1);
			MainCycleRun(); 		
			break;
			case 3:
			RemakeDataArray(&BRUV_Mes, RechangeData, 0, 1);
			MainCycleRun(); 	
			break;
			case 4:
			RemakeDataArray(&BRUV_Mes, RechangeData, 0, 1);
			MainCycleRun(); 		
			break;
			case 5:
			RemakeDataArray(&BRUV_Mes, RechangeData, 0, 1);
			MainCycleRun(); 		
			break;
			case 6:
			RemakeDataArray(&BRUV_Mes, RechangeData, 0, 1);
			MainCycleRun(); 		
			break;
			case 7:
			RemakeDataArray(&BRUV_Mes, RechangeData, 4, 8);
			MainCycleRun(); 		
			break;
			case 8:
			RemakeDataArray(&BRUV_Mes, RechangeData, 4, 8);
			MainCycleRun(); 		
			break;
			default: break;
		}

	}
}
