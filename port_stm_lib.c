#include "port_stm_lib.h"

void GPIO_UnlockKey(GPIO_TypeDef* Port)
{
//	Port -> LCKR = 1+(Port -> LCKR&0xFFFF);
//	Port -> LCKR = 0+(Port -> LCKR&0xFFFF);
//	Port -> LCKR = 1+(Port -> LCKR&0xFFFF);
	
	Port->LCKR |= (1<<16);
	Port->LCKR &= ~(1<<16);
	Port->LCKR |= (1<<16);
}

void GPIO_PinConfig(int Pin, int Mode, int Speed, int FuncNumber,	int Pull,  GPIO_TypeDef* Port)
{
//	if (Port -> LCKR&((1<<16)))
//	{
//		GPIO_UnlockKey(Port);
//	}
	
	Port -> MODER &= ~(0b11<<(Pin*2));
	
	Port -> MODER |= (Mode<<Pin*2);
	
	Port -> OSPEEDR |= (Speed <<Pin*2);
	
	if(Mode == isAlternate)
	{
		if (Pin <= Pin7)
		{	
			Port -> AFR[0] |= (FuncNumber<<(Pin*4));
		}
		else {
						Port -> AFR[1] |= (FuncNumber<<((Pin-8)*4));
					}
	}
	
	
	
	Port -> OTYPER |= (PushPull <<Pin);	
	
	Port -> PUPDR &= (0b11<<(Pin*2));	

	Port -> PUPDR |= (Pull << (Pin*2));	
}

void GPIO_PortConfig(int* Pin, int* Mode, int* Speed, int* FuncNumber, int* Pull,  GPIO_TypeDef* Port, int len)
{
	
	for (int i=0; i < len; i++)
	{
		GPIO_PinConfig( Pin[i], Mode[i], Speed [i], FuncNumber[i], Pull[i], Port);
	}
}

void GPIO_FullPortConfig(int Speed, int PullOrDrain, int Pull, GPIO_TypeDef* Port)
{
//	GPIO_UnlockKey(Port);
	for (int i=0; i<16; i++)
	{
		Port -> MODER |= (isGenPort<<i*2);
		
		Port -> OSPEEDR |= (Speed <<i*2);
		
//		if (i <= 7)
//		{	
//			Port -> AFR[0] |= (FuncNumber<<(i*4));
//		}
//		else {
//						Port -> AFR[1] |= (FuncNumber<<((i-8)*4));
//					}
		
		Port -> OTYPER |= (PullOrDrain <<i);

		Port -> PUPDR |= (Pull << i*2);				
	}
	
}

void GPIO_SetBit(int Pin, GPIO_TypeDef* Port )
{
	Port -> BSRR = (1<<Pin);
}

void GPIO_ResetBit(int Pin, GPIO_TypeDef* Port )
{
	Port -> BSRR = (1<<(Pin+16));
}
