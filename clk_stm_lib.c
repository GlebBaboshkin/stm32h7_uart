#include "stm32h7xx.h"                  // Device header
#include "clk_stm_lib.h"

#define HardwareFreq		25000000			
#define CoreFreq				120000000
#define Prescaler				5
#define MulVCO					240000000/(HardwareFreq/Prescaler)-1    //51 ��������
#define PLLOutDiv				(240000000/CoreFreq)-1									//1 ��������



void PerClkConfig(int PerType)
{
	if ((PerType == _SDMMC1) | (PerType == _QSPIEN) | (PerType == _FMCEN)	| (PerType == _JPEDDC))
	{
		RCC->AHB3ENR |= (1<<PerType);
	}
	else if (PerType == _USB2OTGH | PerType == _USB1OTGSULP | PerType == _USB1OTGH	  |PerType == _USB2OTGSULP |PerType == _ETH1RX |
						PerType == _ETH1EX |PerType == _ETH1MAC |PerType == _ADC12 |PerType == _DMA2 |PerType == _DMA1 )
	{
		RCC->AHB1ENR |= (1<<PerType);
	}
	else if (PerType == _SRAM3 | PerType == _SRAM2 | PerType == _SRAM1	  |PerType == _SDMMC2 |PerType == _RNGEN |
						PerType == _HASH |PerType == _CRYPT |PerType == _DCMI)
	{
		RCC->AHB2ENR |= (1<<PerType);
	}
	else if (PerType == _BKPRAM | PerType == _HSEM | PerType == _ADC3	  |PerType == _BDMA |PerType == _CRC |
						PerType == _PortK |PerType == _PortJ |PerType == _PortI |PerType == _PortH |PerType == _PortG|
						PerType == _PortF|PerType == _PortE|PerType == _PortD| PerType == _PortC| PerType == _PortB| PerType == _PortA)
	{
		RCC->AHB4ENR |= (1<<PerType);
	}
	else if (PerType ==  _WWDG | PerType == _LTDC )
	{
		RCC->APB4ENR |= (1<<PerType); 
	}
	else if (PerType == _UART8 | PerType == _UART7 | PerType == _DAC12	  |PerType == _CEC |PerType == _I2C3 |
						PerType == _I2C2 |PerType == _I2C1 |PerType == _UART5 |PerType == _UART4 |PerType == _USART3|
						PerType == _USART2|PerType == _SPIFRX|PerType == _SPI3| PerType == _SPI2| PerType == _LPTIM1| PerType == _TIM14| 
						PerType == _TIM13| PerType == _TIM12| PerType == _TIM7| PerType == _TIM6|  PerType == _TIM5| PerType == _TIM4|
						PerType == _TIM3|PerType == _TIM2)
	{
		RCC->APB1LENR |= (1<<PerType); 
	}
	else if (PerType == _FDCAN | PerType == _MDIOS | PerType == _OPAMP	  |PerType == _SWP |PerType == _CRS )
	{
		RCC->APB1HENR |= (1<<PerType);
	}	
	else if (PerType == _HRTIM | PerType == _DFSDM1 | PerType == _SAI3	  |PerType == _SAI2 |PerType == _SAI1 |
						PerType == _SPI5 |PerType == _TIM17 |PerType == _TIM16 |PerType == _TIM15 |PerType == _SPI4|
						PerType == _SPI1|PerType == _USART6|PerType == _USART1| PerType == _TIM8| PerType == _TIM12 )
	{
		RCC->APB2ENR |= (1<<PerType); 
	}
	else
	{
		RCC->APB4ENR |= (1<<PerType); 
	}	
}

int SetSystemClk(int ClkType)
{
	if (ClkType > 0b011)
	{
		RCC->CFGR |= 0;
		return 1;
	}
	else {	
					RCC->CFGR |= ClkType;
					return 0;
			 }
}

void CLK_Config()
{
	RCC->CR |= HSEION;
		
	while (!(RCC->CR & HSEIsReady));
		
	RCC->PLLCKSELR = InputIsHSE|(Prescaler << PLL1Divisor); //������������ �� 5, ������� ������� - 5 ���
	
	RCC->PLLCFGR = (from2_to4_MHZ << InputRangePLL1) | (VCOHIsUsed  << PLL1_VCO) |
										(OutEnable << OutR_PLL1) | (OutEnable << OutQ_PLL1) | (OutEnable << OutP_PLL1);
	
	RCC->PLL1DIVR = ((MulVCO) << MulN) | ((PLLOutDiv)<<DivP) | ((PLLOutDiv)<<DivQ) | ((PLLOutDiv)<<DivR);
			
	RCC->CR |= PLL1ON; 
	
	while (!(RCC->CR & PLL1RDY));
	
	int err = SetSystemClk(PLL1_isSysClk); 
	
	if (err == 0)
	{
			RCC->D1CFGR = D1_AHB_PRESCALER1|D1_APB3_PRESCALER1|D1_AHB_PRESCALER1;
		
			RCC->D2CFGR = D2_APB1_PRESCALER1|D2_APB2_PRESCALER1;
			
			RCC->D3CFGR = D3_APB4_PRESCALER1;
	}
}
