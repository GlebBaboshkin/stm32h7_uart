#ifndef __CLK_STM_LIB_H

#define __CLK_STM_LIB_H

/*
	������������ ���� ��� �������� RCC_CR
*/
#define PLL3RDY 	(1<<29)
#define PLL3ON 		(1<<28)
#define PLL2RDY 	(1<<27)
#define PLL2ON	 	(1<<26)
#define PLL1RDY 	(1<<25)
#define PLL1ON		(1<<24)

#define HSESecurityEn 	(1<<19)
#define HSEByPass			 	(1<<18)
#define HSEIsReady			(1<<17)
#define HSEION					(1<<16)

#define Dom2RDY					(1<<15)
#define Dom1RDY					(1<<14)

#define HSI48RDY				(1<<13)
#define HSI48ON					(1<<12)

#define CSIKERON				(1<<9)
#define CSIRDY					(1<<8)
#define CSION						(1<<7)

#define HSIDIVF					(1<<5)
#define HSI_64MHz				(0b00<<3)
#define HSI_32MHz				(0b01<<3)
#define HSI_16MHz				(0b10<<3)
#define HSI_8MHz				(0b11<<3)
#define HSIRDY					(1<<2)
#define HSIKERON				(1<<1)
#define HSION						1

/*
		��������� ������� �� ������ MCO1 � MC02 (RCC_CFGR)
*/

#define MCO2_SysClk			(0b000<<29)
#define MCO2_PLL2				(0b001<<29)
#define MCO2_HSE				(0b010<<29)
#define MCO2_PLL1				(0b011<<29)
#define MCO2_CSI				(0b100<<29)
#define MCO2_LSI				(0b101<<29)

#define DivisorOfMCO2		25
#define DivisorOfMCO1		18

#define MCO1_SysClk			(0b000<<22)
#define MCO1_PLL2				(0b001<<22)
#define MCO1_HSE				(0b010<<22)
#define MCO1_PLL1				(0b011<<22)
#define MCO1_CSI				(0b100<<22)
#define MCO1_LSI				(0b101<<22)

#define DivisorOfRTC		8

#define HSI_isSysClk		0b000
#define CSI_isSysClk		0b001
#define HSE_isSysClk		0b010
#define PLL1_isSysClk		0b011
/*
		������������ ���� ��� ��������� �������� RCC_D1CFGR (����� ������� 1)
*/

#define D1_AHB_PRESCALER1    (0b0000)
#define D1_AHB_PRESCALER2    (0b1000)
#define D1_AHB_PRESCALER4    (0b1001)
#define D1_AHB_PRESCALER8    (0b1010)
#define D1_AHB_PRESCALER16   (0b1011)
#define D1_AHB_PRESCALER64   (0b1100)
#define D1_AHB_PRESCALER128  (0b1101)
#define D1_AHB_PRESCALER256  (0b1110)
#define D1_AHB_PRESCALER512  (0b1111)

#define D1_APB3_PRESCALER1    (0b000<<4)
#define D1_APB3_PRESCALER2    (0b100<<4)
#define D1_APB3_PRESCALER4    (0b101<<4)
#define D1_APB3_PRESCALER8    (0b110<<4)
#define D1_APB3_PRESCALER16   (0b111<<4)

#define D1_CORE_PRESCALER1    (0b0000<<8)
#define D1_CORE_PRESCALER2    (0b1000<<8)
#define D1_CORE_PRESCALER4    (0b1001<<8)
#define D1_CORE_PRESCALER8    (0b1010<<8)
#define D1_CORE_PRESCALER16   (0b1011<<8)
#define D1_CORE_PRESCALER64   (0b1100<<8)
#define D1_CORE_PRESCALER128  (0b1101<<8)
#define D1_CORE_PRESCALER256  (0b1110<<8)
#define D1_CORE_PRESCALER512  (0b1111<<8)

/*
		������������ ���� ��� ��������� �������� RCC_D2CFGR (����� ������� 2)
*/

#define D2_APB1_PRESCALER1    (0b000<<4)
#define D2_APB1_PRESCALER2    (0b100<<4)
#define D2_APB1_PRESCALER4    (0b101<<4)
#define D2_APB1_PRESCALER8    (0b110<<4)
#define D2_APB1_PRESCALER16   (0b111<<4)

#define D2_APB2_PRESCALER1    (0b000<<8)
#define D2_APB2_PRESCALER2    (0b100<<8)
#define D2_APB2_PRESCALER4    (0b101<<8)
#define D2_APB2_PRESCALER8    (0b110<<8)
#define D2_APB2_PRESCALER16   (0b111<<8)

/*
		������������ ���� ��� ��������� �������� RCC_D2CFGR (����� ������� 2)
*/

#define D3_APB4_PRESCALER1    (0b000<<4)
#define D3_APB4_PRESCALER2    (0b100<<4)
#define D3_APB4_PRESCALER4    (0b101<<4)
#define D3_APB4_PRESCALER8    (0b110<<4)
#define D3_APB4_PRESCALER16   (0b111<<4)

/*
		������������ ���� ��� ������ ���������� ������� � ���������� RCC_PLLCKSELR
*/


#define InputIsHSI 	(0b00)
#define InputIsCSI	(0b01)
#define InputIsHSE  (0b10)
#define InputNONE   (0b11)

#define PLL1Divisor 4
#define PLL2Divisor 12
#define PLL3Divisor 20

/*
		������������ ��������� ����������� ������� RCC_PLLCFGR
*/

#define OutEnable 1

#define OutR_PLL3   24
#define OutQ_PLL3   23
#define OutP_PLL3   22

#define OutR_PLL2   21
#define OutQ_PLL2   20
#define OutP_PLL2   19

#define OutR_PLL1   18
#define OutQ_PLL1   17
#define OutP_PLL1   16

#define InputRangePLL3   10
#define InputRangePLL2   6
#define InputRangePLL1   2

#define from1_to2_MHZ		 0b00	
#define from2_to4_MHZ		 0b01
#define from4_to8_MHZ		 0b10
#define from8_to16_MHZ	 0b11

#define VCOHIsUsed   1

#define PLL3_VCO		9
#define PLL2_VCO    5
#define PLL1_VCO		1

/*
	������������ ���� ��� ��������� ��������� �� ������ ������ ����������� (RCC_PLLxDIVR)
*/


#define DivP				9
#define DivQ				16
#define DivR				24
#define MulN				0

/*
	��������� ������ � ������ 1 (RCC_D1CCIPR)
*/

#define PerClkIsHSI			(0b00<<28)
#define PerClkIsCSI			(0b01<<28)
#define PerClkIsHSE			(0b10<<28)

#define SDMMC_PLL1			(0<<16)
#define SDMMC_PLL2			(1<<16)

#define QSPI_HCLK3			(0b00<<4)
#define QSPI_PLL1				(0b01<<4)
#define QSPI_PLL2				(0b10<<4)
#define QSPI_PER				(0b11<<4)

#define FMC_HCLK3				(0b00)
#define FMC_PLL1				(0b01)
#define FMC_PLL2				(0b10)
#define FMC_PER					(0b11)

/*
	��������� ������ � ������ 2 (RCC_D2CCIP1R)
*/

#define SWP_HSI					(1<<31)

#define FDCAN_HSE 			(0b00<<28)
#define FDCAN_PLL1 			(0b01<<28)
#define FDCAN_PLL2 			(0b10<<28)

#define DFSDM1_SysClk		(1<<24)

#define SPD_PLL1				(0b00<<20)
#define SPD_PLL2				(0b01<<20)
#define SPD_PLL3				(0b10<<20)
#define SPD_HSI					(0b11<<20)

#define SPI45_APD				(0b000<<16)
#define SPI45_PLL2			(0b001<<16)
#define SPI45_PLL3			(0b010<<16)
#define SPI45_HSI				(0b011<<16)
#define SPI45_CSI				(0b100<<16)
#define SPI45_HSE				(0b101<<16)

#define SPI123_PLL1			(0b000<<12)
#define SPI123_PLL2			(0b001<<12)
#define SPI123_PLL3			(0b010<<12)
#define SPI123_I2Sext		(0b011<<12)
#define SPI123_PrClk		(0b100<<12)


/*
	��������� ������ � ������ 2 (RCC_D2CCIP2R)
*/

//#define USART16_PCLK			(0b000<<3)
//#define USART16_PLL2			(0b001<<3)
//#define USART16_PLL3			(0b010<<3)
//#define USART16_HSI				(0b011<<3)
//#define USART16_CSI				(0b100<<3)
//#define USART16_LSE				(0b101<<3)


#define USART_PCLK				(0b000)
#define USART_PLL2				(0b001)
#define USART_PLL3				(0b010)
#define USART_HSI					(0b011)
#define USART_CSI					(0b100)
#define USART_LSE					(0b101)

/*
		��������� ��������� �� ���� AHB3 (RCC_AHB3ENR)
*/

#define _SDMMC1			16
#define _QSPIEN			14
#define _FMCEN				12
#define _JPEDDC			5

/*
		��������� ��������� �� ���� AHB1 (RCC_AHB1ENR)
*/

#define _USB2OTGH				27
#define _USB1OTGSULP			26
#define _USB1OTGH				25
#define _USB2OTGSULP			18
#define _ETH1RX					17
#define _ETH1EX					16
#define _ETH1MAC					15
#define _ADC12						5
#define _DMA2						1
#define _DMA1						0

/*
		��������� ��������� �� ���� AHB2 (RCC_AHB2ENR)
*/

#define _SRAM3				31
#define _SRAM2				30
#define _SRAM1				29
#define _SDMMC2			9
#define _RNGEN				6
#define _HASH				5
#define _CRYPT				4
#define _DCMI				0

/*
		��������� ��������� �� ���� AHB4 (RCC_AHB4ENR)
*/

#define _BKPRAM				28
#define _HSEM					25
#define _ADC3 					24
#define _BDMA					21
#define _CRC						19
#define _PortK					10
#define _PortJ					9
#define _PortI					8
#define _PortH					7
#define _PortG					6
#define _PortF					5
#define _PortE					4
#define _PortD					3
#define _PortC					2
#define _PortB					1
#define _PortA					0

/*
		��������� ��������� �� ���� APB3 (RCC_APB3ENR)
*/

#define _WWDG				6
#define _LTDC				6

/*
		��������� ��������� �� ���� APB1 (RCC_APB1LENR)
*/

#define _UART8				31
#define _UART7				30
#define _DAC12				29
#define _CEC					27
#define _I2C3				23
#define _I2C2				22
#define _I2C1				21
#define _UART5				20
#define _UART4				19
#define _USART3			18
#define _USART2			17
#define _SPIFRX			16
#define _SPI3				15
#define _SPI2				14
#define _LPTIM1			9
#define _TIM14				8
#define _TIM13				7
#define _TIM12				6
#define _TIM7				5
#define _TIM6				4
#define _TIM5				3
#define _TIM4				2
#define _TIM3				1
#define _TIM2				0


/*
		��������� ��������� �� ���� APB1 (RCC_APB1HENR)
*/

#define _FDCAN			8
#define _MDIOS			5
#define _OPAMP			4
#define _SWP				2
#define _CRS				1

/*
		��������� ��������� �� ���� APB2 (RCC_APB2ENR)
*/

#define _HRTIM			29
#define _DFSDM1		28
#define _SAI3			24
#define _SAI2			23
#define _SAI1			22
#define _SPI5			20
#define _TIM17			18
#define _TIM16			17
#define _TIM15			16
#define _SPI4			13
#define _SPI1			12
#define _USART6		5
#define _USART1		4
#define _TIM8			1
#define _TIM1			0

/*
		��������� ��������� �� ���� APB4 (RCC_APB4ENR)
*/

#define _SAI4				21
#define _RTCAPB			16
#define _VREF				15
#define _COMP12			14
#define _LPTIM5			12
#define _LPTIM4			11
#define _LPTIM3			10
#define _LPTIM2			9
#define _I2C4				7
#define _SPI6				5
#define _LPUART1			3
#define _SYSCFG			1


/*
		����� ������� ����������
*/
void PerClkConfig(int PerType);

int SetSystemClk(int ClkType);

void CLK_Config(void);


#endif
