#ifndef __PORT_STM_LIB

#define __PORT_STM_LIB

#include "stm32h743xx.h"  

#define Pin0		0
#define Pin1		1
#define Pin2		2
#define Pin3		3
#define Pin4		4
#define Pin5		5
#define Pin6		6
#define Pin7		7
#define Pin8		8
#define Pin9		9
#define Pin10		10
#define Pin11		11
#define Pin12		12
#define Pin13		13
#define Pin14		14
#define Pin15		15

/*
		����� ��� ��������� ������ ������ ����� (GPIOx_MODER)
*/

#define isOnlyInput			0b00
#define isGenPort				0b01
#define isAlternate			0b10
#define isAnalog		`		0b11

/*
		����� ��� ��������� �������� ������ ����� (GPIO_OSPEEDR)
*/

#define isLowSpeed			0b00
#define isMedSpeed			0b01
#define isHighSpeed			0b10
#define isFastFuriouse	0b11

/*
		����� ��� ��������� �������� ����� (GPIO_PUPDR)
*/

#define PullOff					0b00
#define PullUp					0b01
#define PullDown				0b10
#define Reserv					0b11

/*
		����� ������ ��������� ����� (GPIO_OTYPER)
*/

#define PushPull				0b0
#define OpenDrain				0b1

/*
		��������� ������� ��� ������ � �������
*/

void GPIO_UnlockKey(GPIO_TypeDef* Port);

void GPIO_PinConfig(int Pin, int Mode, int Speed, int FuncNumber, int Pull,   GPIO_TypeDef* Port);

void GPIO_PortConfig(int* Pin, int* Mode, int* Speed, int* FuncNumber, int* Pull,  GPIO_TypeDef* Port, int len);

void GPIO_FullPortConfig(int Speed, int PullOrDrain, int Pull, GPIO_TypeDef* Port);

void GPIO_SetBit(int Pin, GPIO_TypeDef* Port );

void GPIO_ResetBit(int Pin, GPIO_TypeDef* Port );

#endif
