#include "stm32h7xx.h"                  // Device header
#include "stm32h743xx.h"
#include "clk_stm_lib.h"
#include "uart_stm_lib.h"


int last, last1;
int UartRxData[22], DataForCPU[22], DataFromCPU[20], RechangeData[16];
int T1=1, T2, T3;
int CRC1, CRC2;
extern int CopyInformation;
int CRC3;
int Regime, BREAK; 


void UART5_IRQHandler()
{
	if (((UART5->ISR&(1<<RXTimeout)) == (1<<RXTimeout)))		//?????????? ?? ????????? ????????? ?????????? ? ????
	{
			last1 = UART_FIFOReader(last1, DataFromCPU, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART5);
			GPIO_SetBit(Pin1, GPIOE); 
			if (DataFromCPU[0] == 21)
			{


				CRC3 = (0b01101001^(DataFromCPU[0] + (~DataFromCPU[1]<<4)))&0xFF;
				
				if (CRC3 == DataFromCPU[2])
				{
						for (int i = 3 ; i < last1-1;i++) 
						{ 
							RechangeData[i-3] = DataFromCPU[i];
						}					
					
					if (Regime != DataFromCPU[1])
					{
						
						Regime = DataFromCPU[1];
					 	last1 = 0;
					}
					else {
								if (Regime != 0) BREAK = 1;
								last1 = 0;
					}
					UART_SendBuffer(DataForCPU, sizeof(DataForCPU)/sizeof(DataForCPU[0]), UART5);	
				}
				else {last1 = 0; }
			}
			else {last1 = 0;}
			CRC3 = 0;
			GPIO_ResetBit(Pin1, GPIOE);
			UART5 -> ICR |= (1<<11);
	}
	
}



void UART4_IRQHandler()
{		


	if (((UART4->ISR&(1<<RXFifoNotEmpty)) == (1<<RXFifoNotEmpty)) && ((UART4->ISR&(1<<RXFifoThresh))!=(1<<RXFifoThresh)))		//?????????? ?? ????????? ????????? ?????????? ? ????
	{//GPIO_SetBit(Pin0, GPIOE);
		
			TIM5->CR1 &= ~(1<<0);
			TIM5->CNT = 0;
		//GPIO_ResetBit(Pin0, GPIOE);
		if (last == 0)																					//????? ??? ?? ???????, ????????? ?????? ????? ?? ????
		{
			//GPIO_SetBit(Pin0, GPIOE);
		
			Sended = 1;																						//????????????? ????????? ???????? ????????? ?? ????
		
			//GPIO_ResetBit(Pin0, GPIOE);
			
			UART4->ICR |= (1<<RXTimeout);
			UART4->CR2 |= ReciveTimeOutEn;												//????????? ?????????? ?? ????????
		
			UART4 -> CR1 &= ~RXFIFONotEmpty_IE;										//????????? ?????????? ?? ??????? ?? ??????? ????
			
			T1=1;																									//????????? ? ????????? ???????? ??????? ??????
			
			return;
		}
		else {					//?????? ????? ???? ?????? ????????????, ????? ? ???? ???? ??????????, ?? ??? ?? ????????? ?? ??????. ?????? ????????? ?? ????????? ????????.


			last = UART_FIFOReader(last, UartRxData, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART4);

			if (T1 == 1 & T2 ==1 & T3 ==1)										//???? ?????? ????? ? ??1, ????? ??????? ??? ?????????? ???????? ? ????
			{

			
				for (int i=3; i<last-1; i++)
				{
					CRC2 +=  (CRC2<<2)+UartRxData[i];
				}
				CRC2 += (CRC2<<2);
				CRC2 ^= (CRC2>>8);
				CRC2 &= 0xFF;
			
				if (CRC2 == UartRxData[last-1])											//???? ??2 ???? ??????? ??????? ? ????????? ?????? ? ?????????
				{

					CRC1=0; CRC2 = 0;															//?????????? ???
					last=0;																				//????????? ????????? ??????
					CopyInformation = 1;													//????????? ????????? ?????????? ??????????
					//GPIO_SetBit(Pin0, GPIOE);											
					TIM5->CR1 |= (1<<0);														//????????? ??????, ????? ?????????? ??????????
					UART4->CR2 |= ReciveTimeOutEn;												//????????? ?????????? ?? ????????
					UART4->ICR |= (1<<RXTimeout);
					//GPIO_ResetBit(Pin0, GPIOE);
					T2 = 0; T3 = 0;
					return;
				}
				else {
					CRC1=0; CRC2 = 0;															//?????????? ???
					last=0;																				//????????? ????????? ??????
					TIM5->CR1 |= (1<<0);														//????????? ??????, ????? ?????????? ??????????
					UART4->CR2 |= ReciveTimeOutEn;												//????????? ?????????? ?? ????????
					UART4->ICR |= (1<<RXTimeout);			
					T2 = 0; T3 = 0;
					CopyInformation = 0;
				}
			}
			
			if (T1 == 1 & T2 == 0 & T3 == 0)									//?? ???? ???????????? ??????
			{

				last = 0;																				//????????? ?? ????, ? ?? ?? ??? ?? ?????????
				TIM5->CR1 |= (1<<0);														//???????? ???????
				CopyInformation = 0;
				return;

			}
			
			if (T1 == 1 & T2 ==1 & T3 == 0)										//????? ??????, ?? ??1 ?? ???????
			{

				last = 0;																				//????????? ?? ????, ? ?? ?? ??? ?? ?????????
				TIM5->CR1 |= (1<<0);														//???????? ???????
				CopyInformation = 0;
				T2=0;
				return;
			}
			
		}
		GPIO_ResetBit(Pin0, GPIOE);
	}
	
	if ((UART4->ISR&(1<<RXFifoThresh))==(1<<RXFifoThresh))
	{
		GPIO_SetBit(Pin1, GPIOE);
		last = UART_FIFOReader(last, UartRxData, sizeof(UartRxData)/ sizeof(UartRxData[0]), UART4);
		GPIO_ResetBit(Pin1, GPIOE);
		if (T1 == 1  & T2 ==0 & T3 == 0)								//????????? ?????? ????? ? ?????? ???????
		{
		 if ((UartRxData[0]&0xFF) == ISU_addr)					//???? ?????? ????? ????????? ?? ???? ????????? ? ??????? ???? (??? ?????? ?? ?????)
		  	{
				
		  		T2 = 1;																		//?????? ? ????????? ?????? ???? ????????? ????, ? ???????? ??1
		
					if ((UART4->CR3 & (0b111<<RXFifoThreshLevel)) == (0b000<<RXFifoThreshLevel))
					{
						return;																	//?????????????? ???????? ????? ??? ?? ????? ? ??????????? ?? ?????? ??????. ???? ????? 
																										// ???? 12 ? ??????, ??????? ???? ?????? ? ???? ?????????? ????? ?????????. ???? 2 ?????, ?????? ???????.
					}
			  }
			  else {
					
			  	//last = 0; 																//? ????????? ??????, ????? ??????????? ????? ?????? ???? ?? ????
					GPIO_SetBit(Pin1, GPIOE);									//????? ?????????? ?? ???????? ?????????, ? ?? ????????? ???? ?????????, ??????? 
																										//?????? ???? ????? ?????????? ?????? ????????? ????, ??? ?????????? ?????? ????
					UART4->CR2 |= ReciveTimeOutEn;												//????????? ?????????? ?? ????????
					UART4->ICR |= (1<<RXTimeout);

					GPIO_ResetBit(Pin1, GPIOE);
			  	return;																	//????? ?? ????? ?????? - ?????? ???????????. ????????? ????? ?? ?????? ? ???? ??? ????? ??????
																										// ???????? ? ???? ?????. ?? ??? ????? ??????? ?????? ????????.
			  }
		}
		
		if (T1 == 1 & T2 ==1 & T3 == 0)							//????? ??????? ??????, ?? ??1 ??? ?? ?????????
		{

			CRC1 = UartRxData[0];
			CRC1 += (CRC1<<2)+(UartRxData[1]&0xFF);
			CRC1 += (CRC1<<2);
			CRC1 ^= (CRC1>>8);
			CRC1 &= 0xFF;

			
			if (CRC1 == (UartRxData[2]&0xFF))					//???? ??????????? ??????????? ????? ??????? ? ???, ??? ?????? ??????? ?????? ?? ????
			{

				T3=1;																		//??????? ? ????????? ??????????????? ?????? ??????????, ? ??????? CRC2
				UART4->CR2 |= ReciveTimeOutEn;												//????????? ?????????? ?? ????????
			  	UART4->ICR |= (1<<RXTimeout);

				return;																	//????? ?????????? ????? ?????????? ???????? ?????
				
			}
			else {
				
			//???? ??1 - ?? ???????
				
				CRC1 = 0;																//?????????? ??1
				
				//last = 0;																//?????????????? ????? ?????? ?? ????????????? ?????? ???? ????.
			  	UART4->CR2 |= ReciveTimeOutEn;												//????????? ?????????? ?? ????????
				UART4->ICR |= (1<<RXTimeout);
					//?????? ?????? ?????? ?????????? ?????????? ?? ???????? ? ????????? ?????? ???? ????. 

				return;																	//????? ?? ?????????? ??????????
			}
			
		}


	}

}
